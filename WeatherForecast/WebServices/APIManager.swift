//
//  APIManager.swift
//  WeatherForecast
//
//  Created by Venky on 10/10/18.
//  Copyright © 2018 Venky Kowshik. All rights reserved.
//


import Foundation
import SwiftyJSON
import Alamofire


class APIManager {
    
    
    //Tries to fetch the Forecast from forecast.io
    func fetchWeatherForecast(callback:@escaping ([WeatherCondition])->()) {
        
        //Empty [WeatherCondition] which is to be filled with data
        var weatherConditions = [WeatherCondition]()
        
        LOLogger.debug("Trying to locate phone...")
        
        //Saves location for API-Use
        let lat = 17.3850
        let lon = 78.486
        
        let prefUnitSet = "us"
       
        
        //Requesting Data from Server
        //Register at developer.forecast.io for your own API-Key. Please don't use this key if you're using this code in your own project.
        
        Alamofire.request("https://api.darksky.net/forecast/a91b273ac65f6e7008339debbd25e63b/\(lat),\(lon)?extend=hourly&units=\(prefUnitSet)", method: .get).validate().responseJSON { response in
            switch response.result {
            case .success(let value):
                let json = JSON(value)
                LOLogger.debug("JSON: \(json)")
                let currDeg = json["currently"]["temperature"].doubleValue
                let currMaxDeg = json["daily"]["data"][0]["temperatureMax"].doubleValue
                let currUnt = json["flags"]["units"].stringValue
                let currIcn = json["currently"]["icon"].stringValue
                let currTim = json["currently"]["time"].stringValue
                let currMinDeg = json["daily"]["data"][0]["temperatureMin"].doubleValue
                let currPrecip = json["daily"]["data"][0]["precipProbability"].doubleValue
                let currWind = json["daily"]["data"][0]["windSpeed"].doubleValue
                let currDesc = json["daily"]["data"][0]["summary"].stringValue
                
                let offset = json["offset"].intValue
                
                //Determines the current Time of the day in order to display the right graph later
                let offsetCalendar = NSCalendar(calendarIdentifier: NSCalendar.Identifier.gregorian)!
                offsetCalendar.timeZone = NSTimeZone(forSecondsFromGMT: offset*60*60) as TimeZone
                
                let currentDate = NSDate(timeIntervalSince1970: TimeInterval(currTim)!).addingTimeInterval(TimeInterval(offset*60*60))
                let tomorrow = currentDate.addingTimeInterval(TimeInterval("86400")!)
                let startOfTomorrow = offsetCalendar.startOfDay(for: tomorrow as Date).addingTimeInterval(TimeInterval(offset*60*60))
                
                let hoursTillMidnight = startOfTomorrow.hoursCount(from: currentDate as Date)
                let hoursPassed = 23 - hoursTillMidnight
                
                
                //Only adds the coming hours, as the past hours are not provided for the current day
                var currTemperatures = [Double](repeating: Double.infinity, count: 24)
                for i in 0...23 {
                    if i >= hoursPassed {
                        currTemperatures[i] = json["hourly"]["data"][i - hoursPassed]["temperature"].doubleValue
                    }
                }
                
                
                weatherConditions.append(WeatherCondition(maxDegrees: currMaxDeg, units: currUnt, icon: currIcn, time: currTim, minDegrees: currMinDeg, windSpeed: currWind, precipitation: currPrecip, currDeg: currDeg, description: currDesc, temperatures: currTemperatures))
                
                for i in 1...5 {
                    let maxDeg = json["daily"]["data"][i]["temperatureMax"].doubleValue
                    let unt = json["flags"]["units"].stringValue
                    let icn = json["daily"]["data"][i]["icon"].stringValue
                    let tim = json["daily"]["data"][i]["time"].stringValue
                    let minDeg = json["daily"]["data"][i]["temperatureMin"].doubleValue
                    let precip = json["daily"]["data"][i]["precipProbability"].doubleValue
                    let wind = json["daily"]["data"][i]["windSpeed"].doubleValue
                    let desc = json["daily"]["data"][i]["summary"].stringValue
                    
                    var temperatures = [Double]()
                    for j in 0...23 { temperatures.append(json["hourly"]["data"][j+(24*i) - hoursPassed]["temperature"].doubleValue) }
                    
                    let weather = WeatherCondition(maxDegrees: maxDeg, units: unt, icon: icn, time: tim, minDegrees: minDeg, windSpeed: wind, precipitation: precip, currDeg: nil, description: desc, temperatures: temperatures)
                    weatherConditions.append(weather)
                }
                
                LOLogger.debug("Success! Calling back WeatherController with data...")
                callback(weatherConditions)
            case .failure(let error):
                LOLogger.debug(error.localizedDescription)
                LOLogger.debug("Error while requesting weather data:")
                LOLogger.debug(response.result.error?.localizedDescription ?? "")
                callback(weatherConditions)
            }
        }
    }
}

extension Date {
    func hoursCount(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self as Date).hour ?? 0
    }
}
