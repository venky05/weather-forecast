//
//  LOLogger.swift
//   WeatherForecast
//
//  Created by Venky on 10/10/18.
//  Copyright © 2018 Venky Kowshik. All rights reserved.
//

import UIKit

struct LOLogger {

    static let _kDefaultForceInProduction: Bool = false


    /// Prints in debug only
    static func debug(_ msg: String, line: Int = #line, fileName: String = #file, funcName: String = #function) {
        #if DEBUG
        let fname = (fileName as NSString).lastPathComponent
        print("[\(fname) \(funcName):\(line)]", msg)
        #endif
    }

    /// Prints an error message in debug only
    static func error(_ msg: String, line: Int = #line, fileName: String = #file, funcName: String = #function) {
        debug("ERROR: \(msg)!!", line: line, fileName: fileName, funcName: funcName)
    }

    /// Prints the debug mark for the line
    static func mark(line: Int = #line, fileName: String = #file, funcName: String = #function) {
        debug("called", line: line, fileName: fileName, funcName: funcName)
    }

    static func log(string: String?, forceInProduction: Bool) {
        guard let str = string else {
            return;
        }

        #if DEBUG
        print(str)
        #endif

        if forceInProduction {
            print(str)
        }
    }

}
