//
//  WeatherViewController.swift
//  WeatherForecast
//
//  Created by Venky on 10/10/18.
//  Copyright © 2018 Venky Kowshik. All rights reserved.
//

import UIKit
import SwiftOverlays

class WeatherViewController: UIViewController {
    
    @IBOutlet weak var day0: WeatherConditionView!
    @IBOutlet weak var day1: WeatherConditionView!
    @IBOutlet weak var day2: WeatherConditionView!
    @IBOutlet weak var day3: WeatherConditionView!
    @IBOutlet weak var day4: WeatherConditionView!
    @IBOutlet weak var day5: WeatherConditionView!
    
    var days: [WeatherConditionView]!
    
    let apiManager = APIManager()
    
    @IBAction func refreshPressed(sender: UIBarButtonItem) {
        refresh()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //Puts all WeatherConditionViews in an array to make them accessible by loops easier
        days = [day0, day1, day2, day3, day4, day5]
        
        setNeedsStatusBarAppearanceUpdate()
        
        //Inserts Background Gradient
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.view.bounds
        
        let color1 = UIColor(red: 1, green: 1, blue: 1, alpha: 0).cgColor as CGColor
        let color2 = UIColor(red: 0, green: 0, blue: 0, alpha: 0.4).cgColor as CGColor
        gradientLayer.colors = [color1, color2]
        
        self.view.layer.insertSublayer(gradientLayer, at: 0)
        
        //Makes Navigation Bar Transparent
        let bar:UINavigationBar! =  self.navigationController?.navigationBar
        bar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        bar.shadowImage = UIImage()
        bar.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        bar.barStyle = .blackTranslucent
        
        refresh()
    }
    
    //Refreshes the Data/UI
    func refresh() {
        
        //Callback for putting received Data in the UI
        let refreshCallback: ([WeatherCondition]) -> Void = { weatherConditions in
            
            LOLogger.debug("Trying to put received data in the UI...")
            
            SwiftOverlays.removeAllBlockingOverlays()
            
            //Checks for empty array and shows error message in case someting went wrong
            if weatherConditions.isEmpty {
                LOLogger.debug("The array is empty! Showing error alert...")
                
                let alert = UIAlertController(title: "Error while refreshing Data", message: "Please check your Internet Connection and Location Settings.", preferredStyle: .alert)
                
                let reloadAction = UIAlertAction(title: "Reload", style: .cancel, handler: { aa in self.refresh() })
                let okayAction = UIAlertAction(title: "Okay", style: .default, handler: nil)
                
                alert.addAction(reloadAction)
                alert.addAction(okayAction)
                
                self.present(alert, animated: true, completion: nil)
                return
            }
            
            //Fills the [WeatherCondition] in the UI
            for i in 0...5 { self.days[i].weatherCondition = weatherConditions[i] }
            
            self.view.backgroundColor = weatherConditions[0].color
            LOLogger.debug("Successfully filled UI with refreshed Data!")
            
        }
        
        //Calling the API-Manager for fresh Data
        SwiftOverlays.showBlockingWaitOverlayWithText("Refreshing...")
        apiManager.fetchWeatherForecast(callback: { weatherConditions in refreshCallback(weatherConditions)})
    }
}

