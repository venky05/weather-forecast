
### Project

Sample Weather forecast App which displays current 5 day weather forecast.


### Features

- Clean & Simple UI
- 24h-forecast for the next 5 days
- Forecast data provided by forecast.io


### TODO

- Location Based weather forecast
- City Based weather forecast.
- Detail Screen For Each Day.
- Seetings For Units 
- Notifications on Weather Change
- Graph Repersentation for weather forecast.

### Used Resources

App uses the [forecast.io](https://forecast.io) API for weather data.  The weather icons are provided by [weathericons.io](http://weathericons.io).

It also uses several pods, including Alamofire, SwiftyJSON and  SwiftOverlaysh.

### Notes by Developer

In case you want to improve this app, feel free to create an issues or a Pull Request.

### Build Notes

To build this project, just install the required pods by running the following command in the project directory: 

    pod install


### License
This project is licensed under the MIT License, see [here](https://opensource.org/licenses/MIT) for more information.